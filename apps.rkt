#lang at-exp racket @; -*- Scheme -*-

(provide
  (all-defined-out))

(require
 scribble/html)

(define (info name status type)
  (div class: 'title
    (h3 name)
    (div class: 'meta
      (span (strong "Status: ") status)
      (span (strong "Type: ") type))))
(define (app name status type source)
  (div class: 'app
    (info name status type)
    (hr)
    (pre source)))

(define coin-flip
  (app "Coin Flip" "Ready" "Multi-party"
"#lang glow

@interaction([A, B])
let coinFlip = (wagerAmount, escrowAmount) => {
    // @A assert! canReach(A_wins);
    @A let randA = randomUInt256();
    @verifiably!(A) let commitment = digest(randA);
    publish! A -> commitment;
    deposit! A -> wagerAmount + escrowAmount;

    // @B assert! canReach(B_wins);
    @B let randB = randomUInt256();
    publish! B -> randB;
    deposit! B -> wagerAmount;
    publish! A -> randA;
    verify! commitment;

    if (((randA ^^^ randB) &&& 1) == 0) {
        //A_wins:
        withdraw! A <- 2*wagerAmount + escrowAmount
    } else {
        //B_wins:
        withdraw! B <- 2*wagerAmount;
        withdraw! A <- escrowAmount
    }
};"))

(define buy-signature
  (app "Buy Signature" "Ready" "Multi-party"
"#lang glow

@interaction([Buyer, Seller])
let buySig = (digest : Digest, price : Nat) => {
  deposit! Buyer -> price;

  @publicly!(Seller) let signature = sign(digest);
  // The line above is equivalent to the three below:
  //// @verifiably!(Seller) let signature = sign(digest);
  //// publish! Seller -> signature;
  //// verify! signature; // This line is itself the same as the one below:
  ////// require! isValidSignature(Seller, signature, digest);

  withdraw! Seller <- price;
};"))

(define rock-paper-scissors
  (app "Rock, Paper, Scissors" "Ready" "Multi-party"
"#lang glow

let winner = (handA : Nat, handB : Nat) : Nat => {
    (handA + (4 - handB)) % 3
};

@interaction([A, B])
let rockPaperScissors = (wagerAmount) => {
    @A let handA = input(Nat, \"First player, pick your hand: 0 (Rock), 1 (Paper), 2 (Scissors)\");
    @A require! (handA < 3);
    @A let salt = randomUInt256();
    @verifiably!(A) let commitment = digest(salt, handA);
    publish! A -> commitment;
    deposit! A -> wagerAmount;

    @B let handB = input(Nat, \"Second player, pick your hand: 0 (Rock), 1 (Paper), 2 (Scissors)\");
    publish! B -> handB;
    deposit! B -> wagerAmount;
    require! (handB < 3);

    publish! A -> salt, handA;
    require! (handA < 3);
    verify! commitment;
    // outcome: 0 (B_Wins), 1 (Draw), 2 (A_Wins)
    let outcome = winner(handA, handB);

    switch (outcome) {
      | 0 => withdraw! B <- 2*wagerAmount
      | 1 => withdraw! A <- wagerAmount;
      | 2 => withdraw! A <- 2*wagerAmount
      withdraw! B <- wagerAmount
    };

    outcome
};"))

(define asset-swap (app "Asset Swap" "In development" "Multi-party"
"#lang glow

@interaction({participants: [A, B], assets: [T, U]})
let swap = (t: Nat, u: Nat) => {
  deposit! A -> { T: t };
  deposit! B -> { U: u };
  withdraw! B <- { T: t };
  withdraw! A <- { U: u };
};

// Usage example:
/*
@interaction({participants: [alice, bob], assets: [ETH, DAI]}) swap(10, 2084)
*/"))

(define crowdfunding (app "Crowdfunding" "In development" "Social"
"#lang glow

data Action = Pledge(TokenAmount) | Close | Reclaim(TokenAmount);

let platformCommission amount = quotient(amount, 100);

@interaction
let crowdfunding = (target: TokenAmount,
                    expirationTime : Timestamp) => {
   require! expirationTime > currentTime();

   let rec crowdfund = (ledger : Table(TokenAmount <- Participant),
                        totalPledged: TokenAmount) => {
     assert! totalPledged == totalAmount(ledger);
     choice {
       | ForAllParticipant (NewPledger) {
           @NewPledger amount =
             input([\"Enter next pledge\"], TokenAmount);
           publish! NewPledger -> Pledge(amount);
           deposit! NewPledger -> amount;
           require! currentTime() < expirationTime;
           crowdfund(Table.add(ledger, NewPledger, amount),
                     totalPledged + amount);

       | publish! Organizer -> Success;
           require! currentTime() >= expirationTime;
           require! totalPledged >= target;
           let commission = platformCommission(totalPledged);
           withdraw! Platform <- commission;
           withdraw! Organizer <- totalPledged - commission;

       | ForAllParticipant(Pledger)
           publish! Pledger -> Reclaim(amount);
           require! currentTime() >= expirationTime;
           require! totalPledged < target;
           require! Table.get(ledger, Pledger) == amount;
           withdraw! Pledger <- amount;
           crowdfund(Table.remove(ledger, Pledger),
                     totalPledged - amount);
   }
   crowdfund({}, 0);
}"))

(define auction (app "Auction" "In development" "Social"
"#lang glow

data Action = Bid(TokenAmount) | BuyItNow | Close;

@interaction([Seller])
let simpleAuction = (goods : Assets, expirationTime : Timestamp, buyItNowPrice: TokenAmount) => {
   require! Assets.is_positive(goods);
   require! expirationTime > currentTime();
   deposit! Seller -> goods; // escrow for the goods (TODO: also allow auction of services with an escrow)

   @interaction([Seller, CurrentBidder])
   let rec auction = (currentBid) => {
     assert! @deposited == goods + currentBid;
     choice {
       | ForAllParticipant (NewBidder) {
           @NewBidder bid = Bid(input([\"Enter next bid\"], TokenAmout));
           publish! NewBidder -> bid ; deposit! NewBidder -> bid;
           @NewBidder assume! @value(goods) > @value(bid);
           require! currentTime() < expirationTime;
           require! bid > currentBid;
           //optional: require! bid < buyItNowPrice;
           withdraw! CurrentBidder <- currentBid;
           @interaction([Seller, NewBidder]) auction(bid);
       | ForAllParticipant (NewBidder) {
           publish! NewBidder -> BuyItNow ; deposit! NewBidder -> buyItNowPrice;
           //optional: require! currentTime() < expirationTime;
           withdraw! NewBidder <- goods;
           withdraw! CurrentBidder <- currentBid;
           withdraw! Seller <- buyItNowPrice;
      | { publish! _ -> Close; } => // only Seller and currentBidder are interested in it.
         require! currentTime() >= expirationTime;
         withdraw! Seller <- currentBid;
         withdraw! CurrentBidder <- goods;
     };
   @interaction([Seller, Seller]) auction(0);
}"))

(define checkmate (app "Checkmate" "In development" "Mechanical"
"#lang glow
// Inspired by https://github.com/KarolTrzeszczkowski/Electron-Cash-Last-Will-Plugin

data Command = Withdraw(x : Nat) | Inherit(x : Nat)

@interaction([Owner, Heir])
let deadManSwitch = (expirationDelay) => {
   let rec loop = (expirationTimestamp) =>
     choice {
     | deposit! _ -> x ;
       loop (expirationTimestamp);
     | publish! Owner -> Withdraw(x);
       withdraw! Owner <- x ;
       loop (now() + expirationDelay);
     | publish! Heir -> Inherit(x);
       require! now() >= expirationTimestamp;
       withdraw! Heir <- x;
       loop (expirationTimestamp); };
   loop(now() + expirationDelay); }"))
(define poker (app "Poker" "In planning" "Multi-party" "#lang glow"))
