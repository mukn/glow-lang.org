var changePage = function() {
  var page = window.location.hash;
  if (page === "#home") {
    $('#applications-page').hide();
    $('#home-page').show();
    $('#applications-nav').removeClass('current');
    $('#home-nav').addClass('current');
  } else if (page === "#applications") {
    $('#home-page').hide();
    $('#applications-page').show();
    $('#home-nav').removeClass('current');
    $('#applications-nav').addClass('current');
  }
};

$(window).on('hashchange', changePage);

$(document).ready(function() {
  changePage();

  $num = $('.app').length;
  $even = $num / 2;
  $odd = ($num + 1) / 2;

  if ($num % 2 == 0) {
    $('.app:nth-child(' + $even + ')').addClass('active');
    $('.app:nth-child(' + $even + ')').prev().addClass('prev');
    $('.app:nth-child(' + $even + ')').next().addClass('next');
  } else {
    $('.app:nth-child(' + $odd + ')').addClass('active');
    $('.app:nth-child(' + $odd + ')').prev().addClass('prev');
    $('.app:nth-child(' + $odd + ')').next().addClass('next');
  }

  $('.app').click(function() {
    $slide = $('.active').width();

    if ($(this).hasClass('next')) {
      $('#carousel').stop(false, true).animate({left: '-=' + $slide});
    } else if ($(this).hasClass('prev')) {
      $('#carousel').stop(false, true).animate({left: '+=' + $slide});
    }

    $(this).removeClass('prev next');
    $(this).siblings().removeClass('prev active next');

    $(this).addClass('active');
    $(this).prev().addClass('prev');
    $(this).next().addClass('next');
  });


  // Keyboard nav
  $('html body').keydown(function(e) {
    if (e.keyCode == 37) { // left
      $('.active').prev().trigger('click');
    }
    else if (e.keyCode == 39) { // right
      $('.active').next().trigger('click');
    }
  });
})
