src = index.rkt
lib = utils.rkt

all: public

.PHONY: all public mrproper clean doc public/docs

public: public/docs public/index.html public/talks/security-through-clarity-2021/index.html

public/index.html: index.rkt
	racket $< > $@.tmp && mv $@.tmp $@ || rm $@.tmp

public/docs:
	mkdir -p public/docs
	raco pkg install --skip-installed --name glow
	scribble --htmls --dest doc scribblings/glow.scrbl
	rsync -av doc/glow/ public/docs/

public/talks/security-through-clarity-2021/index.html: talks/security-through-clarity-2021.md
	mkdir -p public/talks/security-through-clarity-2021/
	markdown < $< > $@

clean:
	rm -f public/index.html
	rm -rf public/docs
	rm -rf doc

mrproper:
	git clean -xfd
