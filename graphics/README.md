# Recipes for the following files

## To create the official standalone version of mukn-name or mukn-icon

 * edit the SVG with Inkscape
 * export the SVG as EPS with the "convert to path" option
 * import again in Inkscape with default option
 * export again as SVG

Alternatively, find a way to script the above into existence.


## To rasterize a picture

 * Rasterizing using Imagemagick, keeping the transparent background:
   ```
   convert -density 1200 mukn-name.svg mukn-name.png
   ```

 * Rasterizing using Imagemagick, forcing a white background, with some borders:
   ```
   convert -density 1200 mukn-icon.svg -background white -flatten -bordercolor white -border 60 mukn-icon-whitebg.png
   ```

 * Rasterizing using Imagemagick, with a border, so you can insert the picture in a printable document:
   ```
   convert -density 1200 mukn-business-card-background.svg -bordercolor '#cccccc' -border 10 b.png
   ```

 * Making a 1128x191 picture for linkedin (ok, only 1127):
   ```
   convert -density 150 mukn-name.svg -bordercolor 'white' -border 398x8 -matte -mattecolor white -flatten mukn-name.png
   ```

## To convert a picture to grayscale

```
wget https://www.seas.harvard.edu/sites/default/files/styles/embedded_image_large/public/images/news/nada-amin-sq.jpg
convert nada-amin-sq.jpg -set colorspace Gray -separate -average nada-amin-bw.jpg
```

## QR Codes

```
qrencode -o mukn.io-qrcode.png -t png https://mukn.io
qrencode -o fare@mukn.io-qrcode.png -t png fare@mukn.io
```

## Join PDF files into one

```
pdfjoin -o fare@mukn.io-business-card.pdf fare@mukn.io-business-card-front.pdf mukn-business-card-background.pdf
```
